# 1.5.0
# gshipley/installcentos commit #94
# openshift/openshift-ansible commit #13481
# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.


# enable ssh into vm
$ssh = <<-SCRIPT
sed -i 's/\#PubkeyAuthentication yes/PubkeyAuthentication yes/g' /etc/ssh/sshd_config
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd.service
SCRIPT

# check for updates
$update = <<-SCRIPT
yum update -y
SCRIPT

# OpenShift prerequisites
$openshift = <<-SCRIPT
yum install vim git docker net-tools epel-release -y && yum install moreutils jq -y
git clone https://github.com/jakyrian/installcentos.git
cd installcentos && mkdir templates && chmod 777 -R templates
sed -i "s/export DOMAIN=$.*/export DOMAIN=$(hostname)/g" install-openshift.sh
sed -i 's/export USERNAME=$.*/export USERNAME=openshift/g' install-openshift.sh
sed -i 's/export PASSWORD=$.*/export PASSWORD=openshift/g' install-openshift.sh
sed -i "s@export IP=$.*@export IP=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1 -d'/')@g" install-openshift.sh
sed -i 's@https://raw.githubusercontent.com/gshipley/installcentos/master@https://raw.githubusercontent.com/jakyrian/installcentos/master@g' install-openshift.sh
sed -i 's@https://github.com/openshift/openshift-ansible.git@https://github.com/jakyrian/openshift-ansible.git@g' install-openshift.sh
sed -i 's/export PVS=$.*/export PVS=false/g' install-openshift.sh
sed -i '/envsubst.*/a rm -f inventory.download' install-openshift.sh
sed -i '/^oc/d' install-openshift.sh
git clone https://github.com/jakyrian/openshift-scripts.git
chmod u+x openshift-scripts/* && chmod 777 openshift-scripts/*
mv openshift-scripts/deploy-cluster.sh . && mv openshift-scripts/new-app.sh /usr/local/bin/new-app && rm -rf openshift-scripts
SCRIPT

# install BIND
$bind = <<-SCRIPT
yum install bind bind-utils -y
SCRIPT


Vagrant.configure("2") do |config|

  config.vm.define "dns" do |dns|
    dns.vm.box = "centos/7"
    dns.vm.hostname = "dns.bt.uk"

    dns.vm.network "private_network", ip: "192.168.50.100"
    dns.vm.network "public_network", bridge: "Realtek PCIe GBE Family Controller", ip: "192.168.0.100"

    dns.vm.provision "shell", inline: $ssh
    dns.vm.provision "shell", inline: $update, run: "always"
    dns.vm.provision "shell", inline: $bind
    
    dns.vm.provider "virtualbox" do |vb|
      vb.name = "DNS Server"
      vb.memory = 512
      vb.cpus = 1
      vb.customize ["modifyvm", :id, "--nicpromisc3", "allow-all"]
    end
  end

  config.vm.define "master" do |master|
    master.vm.box = "centos/7"
    master.vm.hostname = "master.bt.uk"

    master.vm.network "private_network", ip: "192.168.50.50"
    master.vm.network "public_network", bridge: "Realtek PCIe GBE Family Controller", ip: "192.168.0.50"

    master.vm.provision "shell", inline: $ssh
    master.vm.provision "shell", inline: $update, run: "always"
    master.vm.provision "shell", inline: $openshift
    
    master.vm.provider "virtualbox" do |vb|
      vb.name = "OpenShift (Master)"
      vb.memory = 2048
      vb.cpus = 2
      vb.customize ["modifyvm", :id, "--nicpromisc3", "allow-all"]
    end
  end

  config.vm.define "private" do |private|
    private.vm.box = "centos/7"
    private.vm.hostname = "private.bt.uk"

    private.vm.network "private_network", ip: "192.168.50.51"
    private.vm.network "public_network", bridge: "Realtek PCIe GBE Family Controller", ip: "192.168.0.51"

    private.vm.provision "shell", inline: $ssh
    private.vm.provision "shell", inline: $update, run: "always"
    
    private.vm.provider "virtualbox" do |vb|
      vb.name = "OpenShift (Private)"
      vb.memory = 1024
      vb.cpus = 1
      vb.customize ["modifyvm", :id, "--nicpromisc3", "allow-all"]
    end
  end

# config.vm.define "public" do |public|
#   public.vm.box = "centos/7"
#   public.vm.hostname = "public.bt.uk"
#
#   public.vm.network "private_network", ip: "192.168.50.52"
#   public.vm.network "public_network", bridge: "Realtek PCIe GBE Family Controller", ip: "192.168.0.52"
#
#   public.vm.provision "shell", inline: $ssh
#   public.vm.provision "shell", inline: $update, run: "always"
#    
#   public.vm.provider "virtualbox" do |vb|
#     vb.name = "OpenShift (Public)"
#     vb.memory = 1024
#     vb.cpus = 1
#     vb.customize ["modifyvm", :id, "--nicpromisc3", "allow-all"]
#   end
# end

end